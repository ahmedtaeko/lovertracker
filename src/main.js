import Vue from 'vue'
import App from './App.vue'

import vuetify from './plugins/vuetify';
import router from './router'

import axios from 'axios';

import Auth from './Auth'


import Notifications from 'vue-notification'

Vue.use(Notifications)

window.axios = axios;

// axios.defaults.baseURL = 'http://127.0.0.1:8000/api';
axios.defaults.baseURL = 'https://dev-team.mongez.app/api';

window.Auth = Auth;




Vue.config.productionTip = false

window.v = new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
