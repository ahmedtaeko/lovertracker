import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './views/Dashboard.vue'
import Projects from './views/Projects.vue'
import Team from './views/Team.vue'

import MapPartial from './views/Map.vue'
import Workers from './views/Workers.vue'
import Orders from './views/Orders.vue'
import Login from './views/Login.vue'


Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/projects',
      name: 'projects',
      component: Projects
    },
    {
      path: '/team',
      name: 'team',
      component: Team
    },
    {
      path: '/map',
      name: 'map',
      component: MapPartial
    },
    {
      path: '/workers',
      name: 'workers',
      component: Workers
    },
    {
      path: '/orders',
      name: 'orders',
      component: Orders
    }


  ]
})
